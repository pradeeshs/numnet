import sys
import os
import glob
from keras import backend as K

sys.path.insert(0, './')
import numnetHelper as helper
sys.path.insert(0, './keras_nets')
import keras_net_simple as ns
import keras_net_deep_elu as  de
import keras_net_deep_leaky as dl
import keras_net_deep_prelu as dp
import keras_net_wide_ELU as we
import keras_net_wide_PRelu as wp
import keras_net_wide_leaky as wl

params = dict()
params['batch_size'] = 16384
params['epochs'] = 200
params['path'] = './'
params['data'] = helper.getData(params['path'])
params['plot_loss_logloss'] = False
params['print_history_summary'] = True
params['save_pred'] = True
params['check_eras'] = False
x_train, y_train, x_valid, y_valid, x_predict, ids = helper.getData(params['path'])
params['data_in_dim'] = len(x_train[0])
nets = [ns, de, dl, dp, we, wp, wl]
for net in nets:
    net.runNet(params)
    K.clear_session()
    
    