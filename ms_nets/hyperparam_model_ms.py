import csv
import tf_net_simple_ms
import random
import time
import numpy as np
import pandas as pd
random.seed(time.time())

start = time.time()
np.random.seed(0)

max_steps = 20000
batch_size = 100 
learning_rate = 0.1
decay_rate = 0.1
max_layer = 5
layer_growth_size = 5
layer_growth_factor = 2
model = np.array([],  dtype=np.int32)
count = 0
total_list = []

num_tests = 1

for i in range(max_layer):
	model = []
	for x in range(i+1):
		model = np.append(model, 1)
		model = model * layer_growth_factor
		
	for j in range(layer_growth_size):
		model = (model * layer_growth_factor).astype(int)
		print "*************TEST NUMBER " + str(num_tests) + " OUT OF " + str(max_layer * layer_growth_size) + '*************'
		logloss, acc = tf_net_simple_ms.wrapper(learning_rate, batch_size, max_steps, decay_rate, model)
		total_list.append([logloss, acc, len(model), str(model)])
		num_tests += 1
		stop = time.time()
		minutes = (stop - start)//(60)
		sec = ((stop - start)%60)//1
		print '%d min %d sec' % (minutes, sec)


total_list.sort()


print 'Writing values to csv file...'
results_df = pd.DataFrame(data=total_list)
results_df.to_csv("mod_res.csv", index = False, header = ["logloss", "acc", "number of layers", "model"])


stop = time.time()
minutes = (stop - start)//(60)
sec = ((stop - start)%60)//1
print '%d min %d sec' % (minutes, sec)