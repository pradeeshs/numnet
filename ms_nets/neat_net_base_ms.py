"""
2-input XOR example -- this is most likely the simplest possible example.
"""

from __future__ import print_function
import os
import neat
import visualize
import time
import pandas as pd
import numpy as np
import random 
import math

#separate dataset
x_train = []
y_train = []
x_valid = []
y_valid = []
x_test = []
data_path = "../dataset_7_27/"
batch_size = 300
gens = 1000
'''
def eval_genomes(genomes, config):
    for genome_id, genome in genomes:
        genome.fitness = 4.0
        net = neat.nn.FeedForwardNetwork.create(genome, config)
        for xi, xo in zip(xor_inputs, xor_outputs):
            output = net.activate(xi)
            genome.fitness -= (output[0] - xo[0]) ** 2
'''
'''
def eval_genomes(genomes, config):
    for genome_id, genome in genomes:
        correct_cout = 0
        net = neat.nn.FeedForwardNetwork.create(genome, config)
        x_batch, y_batch = getBatch(batch_size, x_train, y_train)
        for x_in, y_in in zip(x_batch, y_batch):
            output = net.activate(x_in)
            if(abs(y_in - output[0])<.5):
                correct_cout += abs(y_in - output[0])            

            #correct_cout -= math.log(abs(y_in - output[0]))
        genome.fitness = correct_cout / batch_size
'''
'''
def eval_genomes(genomes, config):
    for genome_id, genome in genomes:
        correct_cout = 0
        net = neat.nn.FeedForwardNetwork.create(genome, config)
        x_batch, y_batch = getBatch(batch_size, x_train, y_train)
        for x_in, y_in in zip(x_batch, y_batch):
            output = net.activate(x_in)
            correct_cout += math.log(abs(y_in - output[0])+.0000000000001)
        genome.fitness = correct_cout*1000 / batch_size
'''
#'''
def eval_genomes(genomes, config):
    for genome_id, genome in genomes:
        correct_cout = 0
        log_loss = 0
        net = neat.nn.FeedForwardNetwork.create(genome, config)
        x_batch, y_batch = getBatch(batch_size, x_train, y_train)
        for x_in, y_in in zip(x_batch, y_batch):
            output = net.activate(x_in)
            if(abs(y_in - output[0])<.5):
                correct_cout += 1
            #log_loss += math.log(abs(y_in - output[0])+.0000000000001)
        #genome.fitness = (log_loss*1000)/batch_size+ (correct_cout)
        genome.fitness = correct_cout
#'''


def getBatch(size, x_in, y_in):
    length = len(x_in) - 1
    x_out = []
    y_out = []
    if len(x_in) != len(y_in):
        raise ValueError("invalid dataset: data/pred len don't match", len(x_in), len(y_in))
    for x in range(0, size):
        randnum = random.randint(0, length)
        x_out.append(x_in[randnum])
        y_out.append(y_in[randnum])
    return x_out, y_out

'''
def getBatch(size, x_in, y_in):
    length = len(x_in) - 1
    x_out = []
    y_out = []
    if len(x_in) != len(y_in):
        raise ValueError("invalid dataset: data/pred len don't match", len(x_in), len(y_in))
    randnum = random.randint(0, length - batch_size)
    x_out = (x_in[randnum:randnum + batch_size])
    y_out = (y_in[randnum:randnum + batch_size])
    return x_out, y_out
'''


def setGlobalDataSet(datapath):
    start = time.time()
    random.seed(start)   

    print('Loading data...')
    # Load the data from the CSV files
    training_data = pd.read_csv(datapath + 'numerai_training_data.csv', header=0)
    prediction_data = pd.read_csv(datapath + 'numerai_tournament_data.csv', header=0)

    # Transform the loaded CSV data into numpy arrays
    features = [f for f in list(training_data) if "feature" in f]
    X = training_data[features]
    Y = training_data["target"]
    x_prediction = prediction_data[features]
    ids = prediction_data["id"]

    #get validation data
    label = prediction_data["data_type"]
    label_list = label.values.tolist()
    val_cout = 0
    for item in label_list:
      if item == "validation" :
        val_cout += 1

    #separate dataset
    global x_train, y_train, x_valid, y_valid, x_test
    x_train = X.values.tolist()
    y_train = Y.values.tolist()
    x_valid = x_prediction[0:val_cout].values.tolist()
    y_valid = prediction_data["target"][0:val_cout].values.tolist()
    x_test = x_prediction.values.tolist() 

def run(config_file):
    start = time.time()
    setGlobalDataSet(data_path)
    # Load configuration.
    config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                         neat.DefaultSpeciesSet, neat.DefaultStagnation,
                         config_file)

    # Create the population, which is the top-level object for a NEAT run.
    p = neat.Population(config)

    # Add a stdout reporter to show progress in the terminal.
    p.add_reporter(neat.StdOutReporter(True))
    stats = neat.StatisticsReporter()
    p.add_reporter(stats)
    p.add_reporter(neat.Checkpointer(5))

    # Run for up to 300 generations.
    winner = p.run(eval_genomes, gens)

    # Display the winning genome.
    print('\nBest genome:\n{!s}'.format(winner))

    # Show output of the most fit genome against training data.
    print('\nOutput:')
    winner_net = neat.nn.FeedForwardNetwork.create(winner, config)
    correct_cout = 0
    log_loss = 0
    count = 0
    results = []
    for x_in, y_in in zip(x_valid, y_valid):
        output = winner_net.activate(x_in)
        results.append(output)
        if(abs(y_in - output[0])<.5):
            correct_cout += 1
        log_loss -= math.log(abs(y_in - output[0])+.0000000000001)
        count += 1
    prec = float(correct_cout)/float(count)
    logloss = float(log_loss)/float(count)
    print('examples: %d correct: %d  Precision: %0.05f Logloss: %0.06f' %
        (count, correct_cout, prec, logloss))
    r_mean = np.mean(results)
    r_std = np.std(results)
    print('%0.05f mean, %0.05f std' % (r_mean, r_std))
    stop = time.time()
    minu = (stop - start)//(60)
    sec = ((stop - start)%60)//1
    print(('%d min %d sec') % (minu, sec))
    node_names = {-1:'1', 
        -2: '2',
        -3: '3',
        -4: '4',
        -5: '5',
        -6: '6',
        -7: '7',
        -8: '8',
        -9: '9',
        -10: '10',
        -11:'11', 
        -12: '12',
        -13: '13',
        -14: '14',
        -15: '15',
        -16: '16',
        -17: '17',
        -18: '18',
        -19: '19',
        -20: '20',
        -21: '21', 
        0:'Prediction'}
    visualize.draw_net(config, winner, True, node_names=node_names)
    visualize.plot_stats(stats, ylog=False, view=True)
    visualize.plot_species(stats, view=True)




if __name__ == '__main__':
    # Determine path to configuration file. This path manipulation is
    # here so that the script will run successfully regardless of the
    # current working directory.
    local_dir = os.path.dirname(__file__)
    config_path = os.path.join(local_dir, 'config-numnet-base-ms')
    run(config_path)