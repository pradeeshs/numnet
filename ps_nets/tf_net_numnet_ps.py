#!/usr/bin/env python

import pandas as pd
import numpy as np
import tensorflow as tf
import time
import math
import sys
import os.path
import csv
import random 
from z3c.batching.batch import Batch
random.seed(time.time())

#initialize variables
hl1 = 2000
hl2 = 1500
hl3 = 1000
hl4 = 750
hl5 = 500
hl6 = 250
hl7 = 125
hl8 = 100
hl9 = 75
hl10 = 50
input_layer = 21
num_classes = 1
learning_rate = .001
batch_size = 100
log_dir = 'log_ps'
max_steps = 2000
epochs = 30
dropout = .7
decay_rate = .001



def placeholder_inputs():
    features_placeholder = tf.placeholder(tf.float32, shape=[None, input_layer])
    target_placeholder = tf.placeholder(tf.int32, shape=[None])
    dropout = tf.placeholder(tf.float32)
    is_training = tf.placeholder(tf.bool)
    return features_placeholder, target_placeholder, dropout, is_training

#NO DROPOUT OR BATCHNORM CURRENTLY
def model(features, dropout, is_training):
    # Hidden 1
    with tf.name_scope('hidden1'):
        weights1 = tf.Variable(tf.truncated_normal([input_layer, hl1], stddev=1.0 / math.sqrt(float(input_layer))), name='weights')
        biases1 = tf.Variable(tf.fill([hl1], 1.0), name='biases')
        hidden1 = tf.contrib.layers.batch_norm(tf.matmul(features, weights1) + biases1, is_training = is_training)
        hidden1 = tf.nn.dropout(tf.nn.tanh(hidden1), dropout)
        #hidden1 = tf.nn.relu(tf.matmul(hidden1, weights1) + biases1)
    # Hidden 2
    with tf.name_scope('hidden2'):
        weights2 = tf.Variable(tf.truncated_normal([hl1, hl2], stddev=1.0 / math.sqrt(float(hl1))), name='weights')
        biases2 = tf.Variable(tf.fill([hl2], 1.0), name='biases')
        hidden2 = tf.contrib.layers.batch_norm(tf.matmul(hidden1, weights2) + biases2, is_training = is_training)
        hidden2 = tf.nn.dropout(tf.nn.tanh(hidden2), dropout)
        #hidden2 = tf.nn.relu(tf.matmul(hidden2, weights2) + biases2)
    # Hidden 3
    with tf.name_scope('hidden3'):
        weights3 = tf.Variable(tf.truncated_normal([hl2, hl3], stddev=1.0 / math.sqrt(float(hl2))), name='weights')
        biases3 = tf.Variable(tf.fill([hl3], 1.0), name='biases')
        hidden3 = tf.contrib.layers.batch_norm(tf.matmul(hidden2, weights3) + biases3, is_training = is_training)
        hidden3 = tf.nn.dropout(tf.nn.tanh(hidden3), dropout)
        #hidden3 = tf.nn.relu(tf.matmul(hidden3, weights3) + biases3)
    # Hidden 4
    with tf.name_scope('hidden4'):
        weights4 = tf.Variable(tf.truncated_normal([hl3, hl4], stddev=1.0 / math.sqrt(float(hl3))), name='weights')
        biases4 = tf.Variable(tf.fill([hl4], 1.0), name='biases')
        hidden4 = tf.contrib.layers.batch_norm(tf.matmul(hidden3, weights4) + biases4, is_training = is_training)
        hidden4 = tf.nn.dropout(tf.nn.tanh(hidden4), dropout)
        #hidden4 = tf.nn.relu(tf.matmul(hidden4, weights4) + biases4)
    # Hidden 5
    with tf.name_scope('hidden5'):
        weights5 = tf.Variable(tf.truncated_normal([hl4, hl5], stddev=1.0 / math.sqrt(float(hl4))), name='weights')
        biases5 = tf.Variable(tf.fill([hl5], 1.0), name='biases')
        hidden5 = tf.contrib.layers.batch_norm(tf.matmul(hidden4, weights5) + biases5, is_training = is_training)
        hidden5 = tf.nn.dropout(tf.nn.tanh(hidden5), dropout)
        #hidden5 = tf.nn.relu(tf.matmul(hidden5, weights5) + biases5)
    # Hidden 6
    with tf.name_scope('hidden6'):
        weights6 = tf.Variable(tf.truncated_normal([hl5, hl6], stddev=1.0 / math.sqrt(float(hl4))), name='weights')
        biases6 = tf.Variable(tf.fill([hl6], 1.0), name='biases')
        hidden6 = tf.contrib.layers.batch_norm(tf.matmul(hidden5, weights6) + biases6, is_training = is_training)
        hidden6 = tf.nn.dropout(tf.nn.tanh(hidden6), dropout)
        #hidden6 = tf.nn.relu(tf.matmul(hidden6, weights6) + biases6)
    # Hidden 7
    with tf.name_scope('hidden7'):
        weights7 = tf.Variable(tf.truncated_normal([hl6, hl7], stddev=1.0 / math.sqrt(float(hl4))), name='weights')
        biases7 = tf.Variable(tf.fill([hl7], 1.0), name='biases')
        hidden7 = tf.contrib.layers.batch_norm(tf.matmul(hidden6, weights7) + biases7, is_training = is_training)
        hidden7 = tf.nn.dropout(tf.nn.tanh(hidden7), dropout)
        #hidden7 = tf.nn.relu(tf.matmul(hidden7, weights7) + biases7)
    # Hidden 8
    with tf.name_scope('hidden8'):
        weights8 = tf.Variable(tf.truncated_normal([hl7, hl8], stddev=1.0 / math.sqrt(float(hl4))), name='weights')
        biases8 = tf.Variable(tf.fill([hl8], 1.0), name='biases')
        hidden8 = tf.contrib.layers.batch_norm(tf.matmul(hidden7, weights8) + biases8, is_training = is_training)
        hidden8 = tf.nn.dropout(tf.nn.tanh(hidden8), dropout)
        #hidden8 = tf.nn.relu(tf.matmul(hidden8, weights8) + biases8)
    # Hidden 9
    with tf.name_scope('hidden9'):
        weights9 = tf.Variable(tf.truncated_normal([hl8, hl9], stddev=1.0 / math.sqrt(float(hl4))), name='weights')
        biases9 = tf.Variable(tf.fill([hl9], 1.0), name='biases')
        hidden9 = tf.contrib.layers.batch_norm(tf.matmul(hidden8, weights9) + biases9, is_training = is_training)
        hidden9 = tf.nn.dropout(tf.nn.tanh(hidden9), dropout)
        #hidden9 = tf.nn.relu(tf.matmul(hidden9, weights9) + biases9)
    # Hidden 10
    with tf.name_scope('hidden10'):
        weights10 = tf.Variable(tf.truncated_normal([hl9, hl10], stddev=1.0 / math.sqrt(float(hl4))), name='weights')
        biases10 = tf.Variable(tf.fill([hl10], 1.0), name='biases')
        hidden10 = tf.contrib.layers.batch_norm(tf.matmul(hidden9, weights10) + biases10, is_training = is_training)
        hidden10 = tf.nn.dropout(tf.nn.tanh(hidden10), dropout)
        #hidden10 = tf.nn.relu(tf.matmul(hidden10, weights10) + biases10)
    # Linear
    with tf.name_scope('softmax_linear'):
        weights = tf.Variable(tf.truncated_normal([hl10, num_classes], stddev=1.0 / math.sqrt(float(hl5))), name='weights')
        biases = tf.Variable(tf.zeros([num_classes]), name='biases')
        logits = tf.nn.sigmoid(tf.matmul(hidden10, weights) + biases)
    
    logits = tf.reshape(logits, [-1])
    return logits

def loss(logits, target):
    #cross_entropy = tf.square(tf.cast(target, tf.float32) - tf.cast(logits, tf.float32))
    error = tf.losses.mean_squared_error(target, logits)
    logloss = tf.losses.log_loss(target, logits, reduction = "weighted_mean")
    #return tf.reduce_mean(cross_entropy, name='xentropy_mean'), logloss
    return error, logloss

def evaluation(logits, targets):
    targets = tf.round(tf.cast(targets, tf.float32))
    logits = tf.round(tf.cast(logits, tf.float32))

    correct = tf.equal(logits, targets)

    return tf.reduce_sum(tf.cast(correct, tf.float32))

def fill_feed_dict(batch, batch_features, batch_target, features_placeholder, target_placeholder, keep, dropout, is_training_placeholder, is_training=True):
    #get next batch
    batch_features = batch_features.batches[batch]
    batch_target = batch_target.batches[batch]

    features_feed = list(batch_features)
    target_feed = list(batch_target)

    feed_dict = {
        features_placeholder: features_feed,
        target_placeholder: target_feed,
        keep: dropout,
        is_training_placeholder: is_training
    }

    return feed_dict

def train(loss):
    tf.summary.scalar('loss', loss)
    global_step = tf.Variable(0, name='global_step', trainable=False)
    decay_learn = tf.train.polynomial_decay(learning_rate, global_step, max_steps, decay_rate)

    optimizer = tf.train.GradientDescentOptimizer(decay_learn)
    train_op = optimizer.minimize(loss, global_step=global_step)

    return train_op

def do_eval(sess, eval_correct,logloss,  features_placeholder, target_placeholder, keep_placeholder, is_training_placeholder, x, y):
    num_examples = len(x)
    feed_dict = {features_placeholder: x, target_placeholder: y, keep_placeholder: 1, is_training_placeholder:False}
    true_count = sess.run(eval_correct, feed_dict=feed_dict)
    precision = float(true_count) / num_examples
    tf.summary.scalar('precision', precision)
    logloss = sess.run([logloss],feed_dict=feed_dict)
    print('examples: %d correct: %d  Precision: %0.05f Logloss: %0.06f' %
        (num_examples, true_count, precision, np.mean(logloss)))

def run_training(xbatch_train, ybatch_train, xbatch_valid, ybatch_valid, xtest, xtrain, ytrain, xvalid, yvalid):
    with tf.Graph().as_default():
        features_placeholder, target_placeholder, keep_placeholder, is_training_placeholder = placeholder_inputs()
        logits = model(features_placeholder, keep_placeholder, is_training_placeholder)
        loss_val, logloss = loss(logits, target_placeholder)
        train_op = train(loss_val)
        eval_correct = evaluation(logits, target_placeholder)
        summary = tf.summary.merge_all()
        init = tf.global_variables_initializer()
        saver = tf.train.Saver()
        sess = tf.Session()
        summary_writer = tf.summary.FileWriter(log_dir, sess.graph)
        sess.run(init)

        batchnum = xbatch_train.total
        step_count = 0
        eval_placeholder = tf.placeholder(tf.float32, shape=[None])

        for i in range(epochs):
            print '******************EPOCH ' + str(i+1) + '******************'
            batch_order = random.sample(range(batchnum),batchnum)

            for step in xrange(batchnum):
                step_count += 1
                batch = batch_order[step]
                #batch = 0
                start_time = time.time()
                feed_dict = fill_feed_dict(batch, xbatch_train, ybatch_train, features_placeholder, target_placeholder, keep_placeholder, dropout, is_training_placeholder, True)
                _, loss_value = sess.run([train_op, loss_val], feed_dict=feed_dict)

                duration = time.time() - start_time
                if step % 100 == 0:
                    logits_to_print =  sess.run([logits],  feed_dict=feed_dict)
                    logmean = np.mean(logits_to_print)
                    logstd = np.std(logits_to_print)
                    print('Step %d: loss = %.5f (%.3f sec), Step Mean: %.5f Step Std Dev: %.7f' % (step, loss_value, duration, logmean, logstd))
                    summary_str = sess.run(summary, feed_dict=feed_dict)
                    summary_writer.add_summary(summary_str, step_count)
                    summary_writer.flush()

            checkpoint_file = os.path.join(log_dir, 'model.ckpt')
            saver.save(sess, checkpoint_file, global_step=step)
            #logits_eval = sess.run([xtrain], feed_dict = feed_dict)
            print('Training Data Eval:')
            do_eval(sess, eval_correct, logloss, features_placeholder, target_placeholder, keep_placeholder, is_training_placeholder, xtrain, ytrain)
            
            #feed_dict = {features_placeholder: xtrain, target_placeholder: ytrain, keep_placeholder: 1, is_training_placeholder: False}
            #true_count = sess.run(eval_correct, feed_dict=feed_dict)
            #print 'get logits'
            #logits_train = model(xtrain, dropout, True)
            #print 'evaluate'
            #true_count = evaluation(logits_train, ytrain)
            #precision = float(true_count)/len(xtrain)
            #print 'TRAINING PRECISION: ' + str(precision)

            print('Validation Data Eval:')
            #do_eval(sess, eval_correct, logloss, features_placeholder, target_placeholder, keep_placeholder, is_training_placeholder, xvalid, yvalid)

        predictions = sess.run(logits, feed_dict = {features_placeholder: xtest, keep_placeholder: 1, is_training_placeholder:False})

        return predictions

def main():
    start = time.time()
    np.random.seed(0)   

    print 'Loading data...'
    # Load the data from the CSV files
    training_data = pd.read_csv('../dataset_7_27/numerai_training_data.csv', header=0)
    prediction_data = pd.read_csv('../dataset_7_27/numerai_tournament_data.csv', header=0)

    # Transform the loaded CSV data into numpy arrays
    features = [f for f in list(training_data) if "feature" in f]
    X = training_data[features]
    Y = training_data["target"]
    x_prediction = prediction_data[features]
    ids = prediction_data["id"]

    #get validation data
    label = prediction_data["data_type"]
    label_list = label.values.tolist()
    val_cout = 0
    for item in label_list:
      if item == "validation" :
        val_cout += 1

    #separate dataset
    x_train = X[1:]
    y_train = Y[1:]
    x_valid = x_prediction[1:val_cout]
    y_valid = prediction_data["target"][1:val_cout]
    x_test = x_prediction 


    #create batches
    batch_features_train = Batch(x_train.values.tolist(), size = batch_size)
    batch_target_train = Batch(y_train.values.tolist(), size = batch_size)
    batch_features_valid = Batch(x_valid.values.tolist(), size = batch_size)
    batch_target_valid = Batch(y_valid.values.tolist(), size = batch_size)

    print 'Training...'
    predictions = run_training(batch_features_train, batch_target_train, batch_features_valid, batch_target_valid, x_test, x_train, y_train, x_valid, y_valid)

    mean = np.mean(predictions)
    std_dev = np.std(predictions)
    print 'Training complete, predictions obtained'
    results_df = pd.DataFrame(data={'probability':predictions})
    joined = pd.DataFrame(ids).join(results_df)

    print "Writing predictions to predictions_ps.csv"
    joined.to_csv("predictions_ps.csv", index=False)
    print "Mean of Predictions:" + str(mean)
    print "Standard Deviation of Predictions: " + str(std_dev)
    print "Done"
    stop = time.time()
    min = (stop - start)//(60)
    sec = ((start-stop)%60)//1
    print('%d min %d sec') % (min, sec)


if __name__ == '__main__':
    main()
