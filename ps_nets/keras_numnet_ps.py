#!/usr/bin/env python

import pandas as pd
import numpy as np
import tensorflow as tf
import time
import math
import sys
import os.path
import csv
import keras
from keras.models import Sequential
from keras.layers import Dense
import random 
from z3c.batching.batch import Batch
random.seed(time.time())

#initialize variables
hl1 = 2000
hl2 = 1500
hl3 = 1000
hl4 = 750
hl5 = 500
hl6 = 250
hl7 = 125
hl8 = 100
hl9 = 75
hl10 = 50
input_layer = 21
num_classes = 1
learning_rate = .001
batch_size = 100
#log_dir = 'log_ps'
#max_steps = 2000
epochs = 15
#dropout = .5
#decay_rate = .00001

#*****READ DATA*****
start = time.time()
print 'Loading data...'
# Load the data from the CSV files
training_data = pd.read_csv('../dataset_7_27/numerai_training_data.csv', header=0)
prediction_data = pd.read_csv('../dataset_7_27/numerai_tournament_data.csv', header=0)

# Transform the loaded CSV data into numpy arrays
features = [f for f in list(training_data) if "feature" in f]
X = training_data[features]
Y = training_data["target"]
x_prediction = prediction_data[features]
ids = prediction_data["id"]

#get validation data
label = prediction_data["data_type"]
label_list = label.values.tolist()
val_cout = 0
for item in label_list:
  if item == "validation" :
    val_cout += 1

#separate dataset
x_train = X[1:]
y_train = Y[1:]
x_train = np.array(x_train)
y_train = np.array(y_train)
x_valid = x_prediction[1:val_cout]
y_valid = prediction_data["target"][1:val_cout]
x_test = x_prediction 

#*****CREATE MODEL*****
model = Sequential()

model.add(Dense(hl1, input_shape = (input_layer,), activation = 'relu'))
model.add(Dense(hl2, activation = 'relu'))
model.add(Dense(hl3, activation = 'relu'))
model.add(Dense(hl4, activation = 'relu'))
model.add(Dense(hl5, activation = 'relu'))
model.add(Dense(hl6, activation = 'relu'))
model.add(Dense(hl7, activation = 'relu'))
model.add(Dense(hl8, activation = 'relu'))
model.add(Dense(hl9, activation = 'relu'))
model.add(Dense(hl10, activation = 'relu'))
model.add(Dense(num_classes, activation = 'sigmoid'))

#model learning
model.compile(optimizer= keras.optimizers.SGD(lr = learning_rate), loss='mean_squared_error', metrics = ['accuracy'])

print type(x_train)
print type(y_train)

#model training
model.fit(x_train, y_train, epochs = epochs, batch_size = batch_size, shuffle= True)

print 'Evaluate model'
#evaluate
score = model.evaluate(x_train, y_train)

print 'Training score = ' + score[0]
print 'Training accuracy = ' + score[1]


def evaluation(logits, targets):
    targets = tf.round(tf.cast(targets, tf.float32))
    logits = tf.round(tf.cast(logits, tf.float32))

    correct = tf.equal(logits, targets)

    return tf.reduce_sum(tf.cast(correct, tf.float32))

# def main():

#     print 'Training...'
#     predictions = run_training(batch_features_train, batch_target_train, batch_features_valid, batch_target_valid, x_test, x_train, y_train, x_valid, y_valid)

#     mean = np.mean(predictions)
#     std_dev = np.std(predictions)
#     print 'Training complete, predictions obtained'
#     results_df = pd.DataFrame(data={'probability':predictions})
#     joined = pd.DataFrame(ids).join(results_df)

#     print "Writing predictions to predictions_ps.csv"
#     joined.to_csv("predictions_ps.csv", index=False)
#     print "Mean of Predictions:" + str(mean)
#     print "Standard Deviation of Predictions: " + str(std_dev)
#     print "Done"
#     stop = time.time()
#     min = (stop - start)//(60)
#     sec = ((start-stop)%60)//1
#     print('%d min %d sec') % (min, sec)